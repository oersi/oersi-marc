default outfile = FLUX_DIR + "output/output.xml";
directory = FLUX_DIR + "test/input/";
createEndTime = "0"; //0 for dummi date with zeros, and 1 for real date. 
default sidreBaseUrl = "https://oersi.org/resources/";

directory
| read-dir
| open-file
| as-records
| decode-json
| fix(FLUX_DIR + "oersiToMarc.fix", *)
| batch-reset(batchsize="1")
| encode-marcxml
| write(FLUX_DIR + "test/output/oersi_testrecord_${i}_output.xml")
;
