#!/bin/sh

JSON_DUMP=$1
MARC_DUMP=$2
SIDRE_BASE_URL=$3
DUMP_DIR=$4

# Marc Dump

# This part of the script runs the metafacture workflow for transforming the local dumpfile to marc and sets the first argument
# for the input file, the second argument for the output file and the third to configure the base url for the sidre links (e.g. oersi.org or develop.oersi.org).
echo "$(date '+%Y/%m/%d %H:%M:%S') - Start creating marc dump"
./metafix-runner/bin/metafix-runner fromJsonDumpWorkflow.flux inputFile="$JSON_DUMP" outputFile="$MARC_DUMP" sidreBaseUrl="$SIDRE_BASE_URL"

# Weekly update and deletion files

# Based on dumps index e.g. "https://oersi.org/resources/dumps/" ($4) 
# this part of the script creates a file with the titles deleted per week (based on the diff file) 
# and extracts the newly added resources from unzipped oersi-marc-xml-dump (that is created in the first part of the script) ($2)
# into an update file (naming convention should be "oersi_1w_newTitles_$(date +"%Y-%m-%d").mrc.xml")
# and all deletions into an deletions file (naming convention should be "oersi_1w_deletedRecords_list-$(date +"%Y-%m-%d").txt") 

echo "$(date '+%Y/%m/%d %H:%M:%S') - Start creating 1w files"
mkdir -p ${DUMP_DIR}/1w_updates
LAST_DIFF_FILE_W1=$(find $DUMP_DIR -type f -name "oer_data_DIFF_1w_*" -printf "%T@\t%p\n" | sort | tail -n 1 | cut -f 2-)
cat $LAST_DIFF_FILE_W1 | sed -r 's/(^-.+\t)(.+$)/oersi:\2/g;/^\+.*/d' > "$DUMP_DIR/1w_updates/oersi_1w_deletedRecords_list-$(date +"%Y-%m-%d").txt" #Creates Weekly Deletionfile
cat $LAST_DIFF_FILE_W1 | sed -r 's/(^\+.+\t)(.+$)/oersi:\2\tnew/g;/^-.+/d' > maps/oersi_updates.tsv
./metafix-runner/bin/metafix-runner extractNewRecordsFromDump.flux inputFile="$MARC_DUMP" outputFile="$DUMP_DIR/1w_updates/oersi_1w_newTitles_$(date +"%Y-%m-%d").mrc.xml" #Creates weekly new titles file.

rm  maps/oersi_updates.tsv

# Four-weekly update and deletion files

# Based on dumps index e.g. "https://oersi.org/resources/dumps/" ($4) 
# this part of the script creates a file with the titles deleted per four weeks (based on the diff file) 
# and extracts the newly added resources from unzipped oersi-marc-xml-dump (that is created in the first part of the script) ($2)
# into an update file (naming convention should be "oersi_4w_newTitles_$(date +"%Y-%m-%d").mrc.xml")
# and all deletions into an deletions file (naming convention should be "oersi_4w_deletedRecords_list-$(date +"%Y-%m-%d").txt") 

echo "$(date '+%Y/%m/%d %H:%M:%S') - Start creating 4w files"
mkdir -p ${DUMP_DIR}/4w_updates
LAST_DIFF_FILE_W4=$(find $DUMP_DIR -type f -name "oer_data_DIFF_4w_*" -printf "%T@\t%p\n" | sort | tail -n 1 | cut -f 2-)
cat $LAST_DIFF_FILE_W4 | sed -r 's/(^-.+\t)(.+$)/oersi:\2/g;/^\+.*/d' > "$DUMP_DIR/4w_updates/oersi_4w_deletedRecords_list-$(date +"%Y-%m-%d").txt" #Creates Weekly Deletionfile
cat $LAST_DIFF_FILE_W4 | sed -r 's/(^\+.+\t)(.+$)/oersi:\2\tnew/g;/^-.+/d' > maps/oersi_updates.tsv
./metafix-runner/bin/metafix-runner extractNewRecordsFromDump.flux inputFile="$MARC_DUMP" outputFile="$DUMP_DIR/4w_updates/oersi_4w_newTitles_$(date +"%Y-%m-%d").mrc.xml" #Creates weekly new titles file.

rm  maps/oersi_updates.tsv

# Delete all files older than 60 days

echo "$(date '+%Y/%m/%d %H:%M:%S') - Delete files older than 60 days"
find ${DUMP_DIR}/1w_updates -mtime +60 \( -name "oersi_1w_deletedRecords_list-*" -o -name "oersi_1w_newTitles_*" \) -exec rm {} \;
find ${DUMP_DIR}/4w_updates -mtime +60 \( -name "oersi_4w_deletedRecords_list-*" -o -name "oersi_4w_newTitles_*" \) -exec rm {} \;
