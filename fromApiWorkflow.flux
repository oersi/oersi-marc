default outfile = FLUX_DIR + "output/api/output_-${i}.xml";
apiCall = "https://oersi.org/resources/api/search/oer_data/_search?q=mainEntityOfPage.provider.name:%22HOOU%22&size=1000";
createEndTime = "1"; //0 for dummi date with zeros, and 1 for real date. 
default sidreBaseUrl = "https://oersi.org/resources/";

apiCall
| open-http
| as-records
| decode-json(recordPath="hits.hits[*]._source")
| fix(FLUX_DIR + "oersiToMarc.fix",*)
| batch-reset(batchsize="1")
| encode-marcxml
| write(FLUX_DIR + "output/api/test-output-${i}.xml")
;