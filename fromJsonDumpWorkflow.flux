default dumpUrl = "https://oersi.de/resources/dumps/oer_data.ndjson";
default inputFile = FLUX_DIR + "test/input/dump/oer_data.ndjson";
default outputFile = FLUX_DIR + "output/dump/oersiToMarc.mrc.xml.gz";
default sidreBaseUrl = "https://oersi.org/resources/";
createEndTime = "1";//0 for dummi date with zeros, and 1 for real date. 


// This outcommented part can be used to locally save the newest JSON DUMP before transforming the data to MARCXML. 
// dumpUrl
// | open-http
// | as-lines
// | write(dumpFile)
// ;

inputFile
| open-file
| as-lines
| decode-json
| fix(FLUX_DIR + "oersiToMarc.fix",*)
| encode-marcxml
// For testing with copressed output file
// | write(FLUX_DIR + "output/dump/oersiToMarc.mrc.xml.gz", compression="gzip")
| write(outputFile)
;