# oersi-marc

[MARC is still not dead yet](https://www.librarianshipstudies.com/2020/05/marc-must-die-15-years-on.html) and libraries want the OER metadata as MARC21 or MARCXML dump instead of JSON for their library and discovery systems.

To provide to provide the metadata for the libraries we are going to transorm the OERSI [AMB](https://dini-ag-kim.github.io/amb/) data into MARCXML.

We are using [Metafacture](https://metafacture.org/) for the transformation from JSON to MARCXML.

This project is an early stage and provides a first draft for the transformation.

# The transformation 

There are three different transformation workflows that are provided as flux-script and can be run with metafacture:
- a) for testing with the testfiles: `localTestWorkflow.flux`
- b) for testing with a larger set fetching from the API: `fromApiWorkflow.flux`
- c) for creating the dump from the OERSI json dump: `fromJsonDumpWorkflow.flux`


All thre workflow use the same fix (`oersiToMarc.fix`) script for the record level transformation mapping the AMB Json Data to Marc. 
At the moment we created a basic transformation for the following Marc elements:

LEADER, 001, 005, 007, 008, 040, 100, 245, 264, 300, 306, 380, 506, 521, 650, 653, 655, 856

We intend to improve our transformation with the following steps:

- compare the transformation to the following recommondation by [OER Metadata Rosetta Stone](https://docs.google.com/document/d/14tYwNEzr1EKMJAEDHVx2BXeyXFCCSCLGea1lCuySkZE/edit#heading=h.xbfhehpfr3vt) and [MHCC OER Template for MARC](https://docs.google.com/spreadsheets/d/1qRvWqGdrRw11zpKDVMWZWM4BIstAN6frklaTbMrvhcs/edit#gid=0). See ticket https://gitlab.com/oersi/oersi-marc/-/issues/11
- let the library community review the transformation and the results
- adjust the transformation so that the result meets the hbz ALMA NZ rules for eResources.

# Setup

You need JAVA 11 or a compatible version for running metafacture.

Download the current Metafacture Fix runner: https://github.com/metafacture/metafacture-fix/releases

# Usage

For testing:

Unix: `./bin/metafix-runner localTestWorkflow.flux`

Windows: `./bin/metafix-runner.bat localTestWorkflow.flux`


For creating the dump (configure the parameters or use the default settings):

Unix: `./bin/metafix-runner fromJsonDumpWorkflow.flux dumpFile="path/to/oer_data.ndjson" outputFile="path/to/oer_data.mrc.xml"`

Windows: `./bin/metafix-runner.bat fromJsonDumpWorkflow fluxdumpFile="path/to/oer_data.ndjson" outputFile="path/to/oer_data.mrc.xml"`


Or use the transform.sh script:

`bash transform.sh "path/to/oer_data.ndjson" "path/to/oer_data.mrc.xml"``