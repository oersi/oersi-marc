default inputFile = FLUX_DIR + "oer_data.mrc.xml";
default outputFile = FLUX_DIR + "oersi_weekly_newTitles.mrc.xml";

inputFile
| open-file
| decode-xml
| handle-marcxml
| fix(FLUX_DIR + "extractNewRecordsFromDump.fix",*)
| encode-marcxml
| write(outputFile)
;